import 'package:bs_assignment/di/injectable.dart';
import 'package:bs_assignment/features/repo/controller/repo_list_controller.dart';
import 'package:bs_assignment/repository/base_repository.dart';
import 'package:get/get.dart';

class RepoItemsListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RepoListController>(() => RepoListController(repository: getIt<BaseRepository>()));
  }
}
