import 'package:bs_assignment/di/injectable.dart';
import 'package:bs_assignment/features/repo/controller/repo_details_controller.dart';
import 'package:bs_assignment/repository/base_repository.dart';
import 'package:get/get.dart';

class RepoDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RepoDetailsController>(() => RepoDetailsController(repository: getIt<BaseRepository>()));
  }
}
