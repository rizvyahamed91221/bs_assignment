import 'package:bs_assignment/core/base/base_view.dart';
import 'package:bs_assignment/core/theme/colors.dart';
import 'package:bs_assignment/core/theme/text.dart';
import 'package:bs_assignment/core/utils/extensions.dart';
import 'package:bs_assignment/core/values/values.dart';
import 'package:bs_assignment/core/widgets/global/input_field/wid_text_field.dart';
import 'package:bs_assignment/features/repo/controller/repo_details_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScreenRepositoryDetails extends BaseView<RepoDetailsController> {
  ScreenRepositoryDetails({super.key});

  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return AppBar(
      title: AppText.headline6("Repository Details", color: AppColor.whiteFFFFFF),
      centerTitle: true,
      elevation: 0,
    );
  }

  @override
  Widget body(BuildContext context) {
    return Obx(() => controller.repoDetails.value.id != null
        ? SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [headerAvatarImage(), bodyDetails()],
            ),
          )
        : const SizedBox.shrink());
  }

  Widget headerAvatarImage() => Container(
        margin: const EdgeInsets.only(top: AppPadding.p20),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          border: Border.all(
            color: Colors.grey,
            width: 0.6,
          ),
          image: DecorationImage(
            image: NetworkImage(controller.repoDetails.value.owner?.avatarUrl ?? ''),
          ),
        ),
        height: 100,
      );

  Widget bodyDetails() => Column(
        children: controller.repoDetails.value
            .toJson()
            .entries
            .map(
              (e) => e.key == "description"
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: AppInputText(
                        initialValue: "${e.value ?? '-'}",
                        labelText: e.key.toDecent(),
                        readOnly: true,
                        multilineAutoExpandable: true,
                        multiline: 3,
                        borderStyle: InputBorderStyle.bordered,
                      ),
                    )
                  : e.key == "owner" || e.key == "id"
                      ? const SizedBox.shrink()
                      : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                          child: AppInputText(
                            initialValue: e.value.toString().isDateTime
                                ? e.value.toString().dateTimeFormat(format: 'MMM dd, yyyy, kk:mm')
                                : "${e.value}",
                            labelText: e.key.toDecent(),
                            readOnly: true,
                          ),
                        ),
            )
            .toList(),
      );
}
