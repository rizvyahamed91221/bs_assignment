import 'package:bs_assignment/core/base/base_view.dart';
import 'package:bs_assignment/core/theme/colors.dart';
import 'package:bs_assignment/core/theme/text.dart';
import 'package:bs_assignment/core/widgets/global/pagination/pagging_view.dart';
import 'package:bs_assignment/core/widgets/user/modal/wid_sb_modal_center.dart';
import 'package:bs_assignment/features/repo/controller/repo_list_controller.dart';
import 'package:bs_assignment/features/repo/presentation/widget/wid_repository_item.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:bs_assignment/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class RepositoryListScreen extends BaseView<RepoListController> {
  RepositoryListScreen({super.key});

  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return AppBar(
      title: AppText.headline6("Repository List", color: AppColor.whiteFFFFFF),
      centerTitle: true,
      elevation: 0,
      actions: [filterActionWidget()],
    );
  }

  @override
  Widget body(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: PagingView<RepositoryItem>(
          pagingController: controller.paginationController.pagingController,
          itemBuilder: (context, item, i) => RepositoryItemCard(
            model: item,
            onPressCard: () => Get.toNamed(AppRoutes.REPO_DETALS, arguments: item.id),
          ),
        ))
      ],
    );
  }

  Widget filterActionWidget() {
    return IconButton(
        onPressed: () => appSBModalCenter(
            header: "Filter By ",
            onPressed: () => controller.onRefresh().then((value) => Get.back()),
            button1: "Apply Filter",
            widContent: Obx(
              () => Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: AppText.bodySmall('Stars'),
                    leading: Radio<String>(
                      value: "stars",
                      activeColor: AppColor.primaryOne4B9EFF,
                      groupValue: controller.sortBy.value,
                      onChanged: (value) {
                        controller.sortBy.value = value ?? 'stars';
                      },
                    ),
                  ),
                  ListTile(
                    title: AppText.bodySmall('Update'),
                    leading: Radio<String>(
                      value: "update",
                      activeColor: AppColor.primaryOne4B9EFF,
                      groupValue: controller.sortBy.value,
                      onChanged: (value) {
                        controller.sortBy.value = value ?? 'update';
                      },
                    ),
                  ),
                ],
              ),
            ),
            content: ''),
        icon: const Icon(
          Icons.filter_alt_outlined,
          size: 25,
        ));
  }
}
