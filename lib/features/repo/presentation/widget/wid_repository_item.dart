import 'package:bs_assignment/core/theme/colors.dart';
import 'package:bs_assignment/core/values/values.dart';
import 'package:bs_assignment/core/widgets/user/wid_list_item_row.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:flutter/material.dart';

class RepositoryItemCard extends StatelessWidget {
  const RepositoryItemCard({
    Key? key,
    required this.model,
    this.onPressCard,
  }) : super(key: key);

  final RepositoryItem model;
  final VoidCallback? onPressCard;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColor.whiteFFFFFF,
      elevation: 0.0,
      child: AnimatedContainer(
        duration: AppDuration.milliseconds300,
        curve: Curves.easeOut,
        //height: isOpen ? 170 : renderBox.size.height,
        width: double.infinity,
        //margin: const EdgeInsets.only(left: AppMargin.m8, right: AppMargin.m8),
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            GestureDetector(
              onTap: onPressCard,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    AppGap.vertical10,
                    Padding(
                      padding: const EdgeInsets.only(left: AppMargin.m15, right: AppMargin.m15),
                      child: WidgetListItemRow(
                        detailsKey: "Name: ",
                        detailsValue: model.name ?? ' - ',
                      ),
                    ),
                    AppGap.vertical4,
                    Padding(
                      padding: const EdgeInsets.only(left: AppMargin.m15, right: AppMargin.m15),
                      child: WidgetListItemRow(
                        detailsKey: "FullName: ",
                        detailsValue: model.fullName ?? ' - ',
                        //isExpand: false,
                      ),
                    ),
                    AppGap.vertical4,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: AppMargin.m15, right: AppMargin.m15),
                          child: WidgetListItemRow(
                            detailsKey: "Total Watch: ",
                            detailsValue: "${model.watchersCount ?? ' - '}",
                            isExpand: false,
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: AppColor.inputTitleColor,
                          size: 18,
                        )
                      ],
                    ),
                    AppGap.vertical10,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
