import 'package:bs_assignment/core/base/base_controller.dart';
import 'package:bs_assignment/core/base/paging_controller.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:bs_assignment/repository/base_repository.dart';
import 'package:get/get.dart';

class RepoListController extends BaseController {
  final BaseRepository repository;

  RepoListController({required this.repository});
  /* ***
   * Initialize Custom paging controller
   * **/
  late CustomPaginationController<RepositoryItem> paginationController = CustomPaginationController<RepositoryItem>(
      onFetchPage: (pageKey, filterText, searchText) async => await getOrderListService(pageKey));

  var sortBy = 'stars'.obs;
  /* ***
   * call data from repo
   * **/
  Future<List<RepositoryItem>> getOrderListService(
    int pageKey,
  ) async {
    return await callDataService(
      repository.getRepositoryList(sortBy.value, pageKey),
    );
  }

  Future<void> onRefresh() async {
    paginationController.refresh(filter: sortBy.value);
  }

  @override
  void onInit() {
    paginationController.initialize();
    super.onInit();
  }

  @override
  void dispose() {
    paginationController.dispose();
    super.dispose();
  }
}
