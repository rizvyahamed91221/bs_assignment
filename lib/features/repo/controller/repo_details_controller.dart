import 'package:bs_assignment/core/base/base_controller.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:bs_assignment/repository/base_repository.dart';
import 'package:get/get.dart';

class RepoDetailsController extends BaseController {
  final BaseRepository repository;

  RepoDetailsController({required this.repository});

  var data = Get.arguments;
  var repoId = 0.obs;
  var repoDetails = RepositoryItem().obs;
  @override
  void onInit() {
    loadData();
    super.onInit();
  }

  void loadData() async {
    repoId.value = data as int;
    await repository.getRepositoryItemDetails(repoId.value).then((value) => repoDetails.value = value);
    repository.setRepositoryHistory(repoId.value, repoDetails.value);
  }
}
