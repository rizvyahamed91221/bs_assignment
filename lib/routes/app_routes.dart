import 'package:bs_assignment/features/repo/bindings/repo_details_bindings.dart';
import 'package:bs_assignment/features/repo/bindings/repo_items_list_bindings.dart';
import 'package:bs_assignment/features/repo/presentation/screen_repository_details.dart';
import 'package:bs_assignment/features/repo/presentation/screen_repository_list.dart';
import 'package:bs_assignment/features/splash/binding/splash_binding.dart';
import 'package:bs_assignment/features/splash/presentation/splash_screen.dart';
import 'package:bs_assignment/main.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppRoutes {
  AppRoutes._();

  //static variables
  static const String INITINAL = '/';
  static const String REPO_LIST = '/REPO_LIST';
  static const String REPO_DETALS = '/REPO_DETALS';

  static final List<GetPage> routes = [
    GetPage(name: INITINAL, page: () => const SplashScreen(), binding: SplashBinding()),
    GetPage(name: REPO_LIST, page: () => RepositoryListScreen(), binding: RepoItemsListBindings()),
    GetPage(name: REPO_DETALS, page: () => ScreenRepositoryDetails(), binding: RepoDetailsBindings()),
  ];

  static generateRoute(RouteSettings settings) {
    return GetPageRoute(page: () => _generatePage(settings), binding: _generateBinding(settings));
  }

  static Widget _generatePage(RouteSettings settings) {
    switch (settings.name) {
      default:
        return Container();
    }
  }

  static Bindings? _generateBinding(RouteSettings settings) {
    switch (settings.name) {
      default:
        return null;
    }
  }
}
