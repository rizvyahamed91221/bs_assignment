import 'dart:async';

import 'package:bs_assignment/datasource/local_data_source/base_local_source.dart';
import 'package:bs_assignment/datasource/local_data_source/constants/hive_constants.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

@singleton
class ImplementBaseLocalDataSource implements BaseLocalDataSource {
  @override
  Future initBoxes(List<String> boxs) async {
    //Hive.ignoreTypeId(0);
    Hive.registerAdapter(RepositoryItemAdapter());
    Hive.registerAdapter(RepositoryOwnerAdapter());
    for (String tag in boxs) {
      await Hive.openBox(tag);
    }
  }

  @override
  RepositoryItem repositoryItem(int key) {
    var box = Hive.box(HiveConstants.REPOBOX.REPO_BOX);
    return box.get(key, defaultValue: null);
  }

  @override
  Future<void> setRepositoryHistory(int key, RepositoryItem history) {
    var box = Hive.box(HiveConstants.REPOBOX.REPO_BOX);
    return box.put(key, history);
  }
}
