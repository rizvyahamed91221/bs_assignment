import 'package:bs_assignment/datasource/local_data_source/i_base_local_data_source.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:injectable/injectable.dart';

@injectable
abstract class BaseLocalDataSource {
  @factoryMethod
  factory BaseLocalDataSource.from() => ImplementBaseLocalDataSource();

  Future initBoxes(List<String> boxs);

  RepositoryItem repositoryItem(int key);
  Future<void> setRepositoryHistory(int key, RepositoryItem history);
}
