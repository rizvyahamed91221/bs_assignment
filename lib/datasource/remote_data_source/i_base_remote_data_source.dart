import 'package:bs_assignment/core/network/dio_client.dart';
import 'package:bs_assignment/core/network/rest_client.dart';
import 'package:bs_assignment/core/utils/endpoints/endpoints.dart';
import 'package:bs_assignment/datasource/remote_data_source/base_remote_data_source.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ImplementBaseRemoteDataSource extends BaseRemoteDataSource {
  // dio instance
  final DioClient _dioClient;

  // rest-client instance
  final RestClient _restClient;

  // injecting dio instance
  ImplementBaseRemoteDataSource(this._dioClient, this._restClient);

  @override
  Future<List<RepositoryItem>> getRepositoryList(String sortBy, int page) async {
    try {
      final res = await _dioClient.get(Endpoints.REPOSITORIES, queryParameters: {
        "q": Endpoints.q,
        "sort": sortBy,
        "order": "desc",
        "page": "$page",
        "per_page": 10,
      });

      return List<RepositoryItem>.from(res["items"].map((e) => RepositoryItem.fromJson(e)).toList());
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<RepositoryItem> getRepositoryItemDetails(int id) async {
    try {
      final res = await _dioClient.get("${Endpoints.REPOSITORIES_DETAILS}/$id");
      return RepositoryItem.fromJson(res);
    } catch (e) {
      rethrow;
    }
  }
}
