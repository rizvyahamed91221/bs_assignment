import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/* ***
 * INTEGER? EXTENSIONS
 * nullToZero() - If Null then returns Zero
 * isNotNull() - If Empty or Null then it returns provided Data
 * toFill(double data) - If Empty or Null then it returns provided Data
 */

extension Integers on int? {
  // If Null then returns Zero
  int get nullToZero => this ?? 0;

  // Check if Not Null or Null
  bool get isNotNull => this != null ? true : false;

  // If Empty or Null then it returns provided Data
  int toDataWhenNullOrEmpty(int data) => this ?? data;
}

/* ***
 * DOUBLE? EXTENSIONS
 * nullToZero() - If Null then returns Zero
 * isNotNull() - If Empty or Null then it returns provided Data
 * toFill(double data) - If Empty or Null then it returns provided Data
 */

extension Doubles on double? {
  // If Null then returns Zero
  double get nullToZero => this ?? 0.0;

  // Check if Not Null or Null
  bool get isNotNull => this != null ? true : false;

  // If Empty or Null then it returns provided Data
  double toDataWhenNullOrEmpty(double data) => this ?? data;
}

/* ***
 * STRING? EXTENSIONS
 * empty() - Makes empty
 * toBlank() - If Null then returns Blank
 * isNotNull() - If Empty or Null then it returns provided Data
 * toAlternative(dynamic alternative) - If empty then add alternative String Data
 * addBefore(dynamic data) - Add (+) String before its original String
 * addAfter(dynamic data) - Add (+) String after its original String
 * toColor() - Returns Color from Hex Code
 * toSubstring({int? start, int? end}) - Alternative modified version of substring()
 * toCapitalize() - Capitalized first letter
 * toSentence() - Make decent sentence filtering Special Characters
 * toDecent() - Make a decent sentence, Capitalized First Letter, Trimmed and Make Sentence
 */

extension Strings on String? {
  // If Null then Blank
  String empty() => '';

  // If Null then Blank
  String toBlank() => this ?? '';

  // Remove  Blank
  String rem(String data) => this != null ? this!.replaceAll(data, '') : '';

  // Remove  Slash
  String remSlash() => rem('/');

  // Check if Not Null or Null
  bool isNotNull() => this != null ? true : false;

  // If Empty or Null then it returns provided Data
  String toDataWhenNullOrEmpty(dynamic data) => toBlank().isEmpty ? '$data' : toBlank();

  // Add (+) String before its original String
  String addBefore(dynamic data) => toBlank().isEmpty ? '' : (data ?? '') + this!;

  // Add (+) String after its original String
  String addAfter(dynamic data) => toBlank().isEmpty ? '' : this! + (data ?? '');

  // Returns Color from Hex Code
  Color toColor() {
    if (this != null) {
      String hex = this!;
      hex = hex.replaceAll('#', '').trim();
      if (hex.length == 6) hex = 'FF$hex';
      return Color(int.parse(hex, radix: 16));
    }
    return Colors.transparent;
  }

  // Custom Trimmer from
  String toSubstring({int? fromStart, int? fromEnd}) {
    if (this != null) {
      String txt = this!.trim();
      return txt.substring((fromStart ?? 0), txt.length - (fromEnd ?? 0));
    }
    return '';
  }

  // Date Time Format
  String dateTimeFormat({String format = 'dd MMM yy, kk:mm', String fallback = ''}) {
    if (this != null && this != '') {
      try {
        DateTime parsedDate = DateTime.parse(this!);
        return DateFormat(format).format(parsedDate);
      } catch (_) {}
    }
    return fallback;
  }

  // Capitalized
  String toCapitalize() {
    if (this != null) {
      if (this!.isNotEmpty && this != '') {
        String txt = this!.trim();
        if (txt.isNotEmpty) return txt[0].toUpperCase() + txt.substring(1);
      }
    }
    return '';
  }

  // Make a decent sentence
  String toSentence() {
    if (this != null) {
      String txt = this!.trim();
      return txt.replaceAll(RegExp('[^A-Za-z0-9,;-_]'), ' ').replaceAll('_', ' ');
    }
    return '';
  }

  // Make a decent sentence
  String toDecent() {
    if (this != null && this != '') {
      String txt = this!.trim();
      return txt.toSentence().toCapitalize();
    }
    return '';
  }

  String toTitleCase() {
    if (this != null) {
      return this!.replaceAll(RegExp(' +'), ' ').split(' ').map((str) => str.toCapitalize()).join(' ');
    }
    return '';
  }
}

/* ***
 * LIST EXTENSIONS
 * // Make a decent sentence, Capitalized First Letter, Trimmed and Make Sentence
 * toDecent(
 *    bool capitalize = false,
 *    bool sentenced = false,
 *    bool decent = false,
 *    String addBefore = '',
      String addAfter = '',
 * )
 *
 * toLimited({required int limit}) - Get limited amount or list items by setting range
 */

extension Lists on List? {
  // Process List Items in a Decent Way
  String toBeautify({
    bool capitalize = false,
    bool sentenced = false,
    bool decent = false,
    String splitter = ', ',
    String addBefore = '',
    String addAfter = '',
  }) {
    String txt = '';
    if (this != null) {
      List list = this!;
      if (list.isNotEmpty) {
        if (list.length == 1) {
          txt = list.toString().replaceAll(RegExp(r"\p{P}", unicode: true), " ").trim();
          if (capitalize) {
            txt = txt.toCapitalize();
          }
        } else {
          txt = list.reduce((value, element) {
            if (sentenced) {
              if (capitalize) {
                return '${value.toString().toDecent()}$splitter${element.toString().toDecent()}';
              } else {
                return '${value.toString().toSentence()}$splitter${element.toString().toSentence()}';
              }
            } else if (decent) {
              return '${value.toString().toDecent()}$splitter${element.toString().toDecent()}';
            } else if (capitalize) {
              return '${value.toString().toCapitalize()}$splitter${element.toString().toCapitalize()}';
            } else {
              return '$value$splitter$element';
            }
          });
        }
      }
    }
    return (txt.isEmpty ? '' : addBefore) + (txt) + (txt.isEmpty ? '' : addAfter);
  }

  // Process List Items by Limit Way
  List toLimited({required int limit}) {
    List list = [];
    if (this != null) {
      for (int i = 0; i < this!.length; i++) {
        if (limit == i) break;
        list.add(this![i]);
      }
    }
    return list;
  }
}

/* ***
 * FILE? EXTENSIONS
 * toBase64
 */

extension Files on File? {
  // If Null then returns Zero
  String toBase64() {
    if (this != null) {
      List<int> imageByteCode;
      imageByteCode = this!.readAsBytesSync();
      return base64Encode(imageByteCode);
    } else {
      return '';
    }
  }
}
