import 'package:bs_assignment/environment/build_config.dart';

class Endpoints {
  Endpoints._();

  // base url
  static var baseUrl = BuildConfig.instance.envVariables.baseUrl;

  // receiveTimeout
  static const int receiveTimeout = 10000;

  // connectTimeout
  static const int connectionTimeout = 10000;

  static const int sendTimeout = 10000;

  static const String source = "";
  static const String q = "Flutter";

  //TODO: Add other settings and url endpoint here
  static const String REPOSITORIES = "/search/repositories";
  static const String REPOSITORIES_DETAILS = "/repositories";
}
