import 'package:bs_assignment/core/theme/colors.dart';
import 'package:bs_assignment/core/theme/text.dart';
import 'package:bs_assignment/core/values/values.dart';
import 'package:bs_assignment/core/widgets/user/modal/wid_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

appSBModalCenter({
  onPressed,
  header = "Confirm cancel?",
  Widget? widContent,
  content = "Your store has not been connected yet. You’ll have to connect your store again if you want to sync your orders. ",
  button1 = "Confirm",
  button2 = "Cancel",
}) =>
    appDialogue(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText.headline5(header),
          AppGap.vertical20,
          widContent ??
              AppText.bodySmall(
                content,
                color: AppColor.darkLightest6C7576,
              ),
          AppGap.vertical20,
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              button2 != null
                  ? TextButton(
                      onPressed: () => Get.back(),
                      child: AppText.headline5(
                        button2,
                        color: AppColor.primaryOne4B9EFF,
                      ),
                    )
                  : Container(),
              AppGap.horizontal36,
              button1 != null
                  ? TextButton(
                      onPressed: onPressed,
                      child: AppText.headline5(
                        button1,
                        color: AppColor.primaryOne4B9EFF,
                      ),
                    )
                  : Container()
            ],
          )
        ],
      ),
    );
