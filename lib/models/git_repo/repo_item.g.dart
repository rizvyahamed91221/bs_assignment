// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repo_item.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RepositoryItemAdapter extends TypeAdapter<RepositoryItem> {
  @override
  final int typeId = 1;

  @override
  RepositoryItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RepositoryItem(
      id: fields[0] as int?,
      name: fields[1] as String?,
      fullName: fields[2] as String?,
      description: fields[3] as String?,
      watchersCount: fields[4] as int?,
      openIssuesCount: fields[5] as int?,
      createdAt: fields[6] as DateTime?,
      updatedAt: fields[7] as DateTime?,
      pushedAt: fields[8] as DateTime?,
      sshUrl: fields[9] as String?,
      cloneUrl: fields[10] as String?,
      owner: fields[11] as RepositoryOwner?,
    );
  }

  @override
  void write(BinaryWriter writer, RepositoryItem obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.fullName)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.watchersCount)
      ..writeByte(5)
      ..write(obj.openIssuesCount)
      ..writeByte(6)
      ..write(obj.createdAt)
      ..writeByte(7)
      ..write(obj.updatedAt)
      ..writeByte(8)
      ..write(obj.pushedAt)
      ..writeByte(9)
      ..write(obj.sshUrl)
      ..writeByte(10)
      ..write(obj.cloneUrl)
      ..writeByte(11)
      ..write(obj.owner);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepositoryItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class RepositoryOwnerAdapter extends TypeAdapter<RepositoryOwner> {
  @override
  final int typeId = 2;

  @override
  RepositoryOwner read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RepositoryOwner(
      id: fields[0] as int?,
      avatarUrl: fields[1] as String?,
      type: fields[2] as String?,
      login: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, RepositoryOwner obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.avatarUrl)
      ..writeByte(2)
      ..write(obj.type)
      ..writeByte(3)
      ..write(obj.login);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepositoryOwnerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RepositoryItem _$RepositoryItemFromJson(Map<String, dynamic> json) =>
    RepositoryItem(
      id: json['id'] as int?,
      name: json['name'] as String?,
      fullName: json['full_name'] as String?,
      description: json['description'] as String?,
      watchersCount: json['watchers_count'] as int?,
      openIssuesCount: json['open_issues_count'] as int?,
      createdAt: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      pushedAt: json['pushed_at'] == null
          ? null
          : DateTime.parse(json['pushed_at'] as String),
      sshUrl: json['ssh_url'] as String?,
      cloneUrl: json['clone_url'] as String?,
      owner: json['owner'] == null
          ? null
          : RepositoryOwner.fromJson(json['owner'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RepositoryItemToJson(RepositoryItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'full_name': instance.fullName,
      'description': instance.description,
      'watchers_count': instance.watchersCount,
      'open_issues_count': instance.openIssuesCount,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'pushed_at': instance.pushedAt?.toIso8601String(),
      'ssh_url': instance.sshUrl,
      'clone_url': instance.cloneUrl,
      'owner': instance.owner,
    };

RepositoryOwner _$RepositoryOwnerFromJson(Map<String, dynamic> json) =>
    RepositoryOwner(
      id: json['id'] as int?,
      avatarUrl: json['avatar_url'] as String?,
      type: json['type'] as String?,
      login: json['login'] as String?,
    );

Map<String, dynamic> _$RepositoryOwnerToJson(RepositoryOwner instance) =>
    <String, dynamic>{
      'id': instance.id,
      'avatar_url': instance.avatarUrl,
      'type': instance.type,
      'login': instance.login,
    };
