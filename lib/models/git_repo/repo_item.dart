import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
part 'repo_item.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
@HiveType(typeId: 1)
class RepositoryItem {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? name;
  @HiveField(2)
  String? fullName;
  @HiveField(3)
  String? description;
  @HiveField(4)
  int? watchersCount;
  @HiveField(5)
  int? openIssuesCount;
  @HiveField(6)
  DateTime? createdAt;
  @HiveField(7)
  DateTime? updatedAt;
  @HiveField(8)
  DateTime? pushedAt;
  @HiveField(9)
  String? sshUrl;
  @HiveField(10)
  String? cloneUrl;
  @HiveField(11)
  RepositoryOwner? owner;

  RepositoryItem(
      {this.id,
      this.name,
      this.fullName,
      this.description,
      this.watchersCount,
      this.openIssuesCount,
      this.createdAt,
      this.updatedAt,
      this.pushedAt,
      this.sshUrl,
      this.cloneUrl,
      this.owner});

  factory RepositoryItem.fromJson(Map<String, dynamic> json) => _$RepositoryItemFromJson(json);
  Map<String, dynamic> toJson() => _$RepositoryItemToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
@HiveType(typeId: 2)
class RepositoryOwner {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? avatarUrl;
  @HiveField(2)
  String? type;
  @HiveField(3)
  String? login;

  RepositoryOwner({this.id, this.avatarUrl, this.type, this.login});

  factory RepositoryOwner.fromJson(Map<String, dynamic> json) => _$RepositoryOwnerFromJson(json);
  Map<String, dynamic> toJson() => _$RepositoryOwnerToJson(this);
}
