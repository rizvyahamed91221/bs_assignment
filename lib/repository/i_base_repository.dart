import 'package:bs_assignment/core/controller/connection_service.dart';
import 'package:bs_assignment/datasource/local_data_source/base_local_source.dart';
import 'package:bs_assignment/datasource/remote_data_source/base_remote_data_source.dart';
import 'package:bs_assignment/datasource/shared_preference_data_source/base_shared_prefrence.dart';
import 'package:bs_assignment/models/git_repo/repo_item.dart';
import 'package:bs_assignment/repository/base_repository.dart';
import 'package:get/get.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ImplementBaseRepository implements BaseRepository {
  // api objects
  final BaseRemoteDataSource _remoteDataSource;

  final BaseLocalDataSource _localDataSource;

  final BaseSharedPreference _sharedPreference;

  // constructor
  ImplementBaseRepository(this._remoteDataSource, this._localDataSource, this._sharedPreference);

  @override
  Future initBoxes(List<String> boxes) {
    return _localDataSource.initBoxes(boxes);
  }

  @override
  Future<RepositoryItem> getRepositoryItemDetails(int id) async {
    return await Get.find<ConnectivityService>().isInternetConnected().then((value) async {
      if (value) {
        return await _remoteDataSource.getRepositoryItemDetails(id);
      } else {
        return _localDataSource.repositoryItem(id);
      }
    });
  }

  @override
  Future<List<RepositoryItem>> getRepositoryList(String sortBy, int page) async {
    return await _remoteDataSource.getRepositoryList(sortBy, page);
  }

  @override
  Future<void> setRepositoryHistory(int key, RepositoryItem history) async {
    return await _localDataSource.setRepositoryHistory(key, history);
  }
}
